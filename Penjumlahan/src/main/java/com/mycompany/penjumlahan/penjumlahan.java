package com.mycompany.penjumlahan;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class penjumlahan extends JFrame implements ActionListener {

    private static final int FRAME_WIDTH = 300;
    private static final int FRAME_HEIGHT = 200;
    private static final int FRAME_X_ORIGIN = 150;
    private static final int FRAME_Y_ORIGIN = 250;
    private static final int BUTTON_WIDTH = 80;
    private static final int BUTTON_HEIGHT = 30;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JTextField text1;
    private JTextField text2;
    private JTextField text3;
    private JButton button_jumlah;

    public static void main(String[] args) {
        penjumlahan frame = new penjumlahan();
        frame.setVisible(true);

    }

    public penjumlahan() {
        Container contentPane = getContentPane();
        contentPane.setLayout(null);

        setSize(FRAME_WIDTH, FRAME_HEIGHT);
        setResizable(false);
        setTitle("Input Data");
        setLocation(FRAME_X_ORIGIN, FRAME_Y_ORIGIN);

        label1 = new JLabel("Bilangan 1");
        label1.setBounds(20, 5, 110, 30);
        contentPane.add(label1);

        text1 = new JTextField();
        text1.setBounds(120, 5, 120, 20);
        contentPane.add(text1);

        label2 = new JLabel("Bilangan 2");
        label2.setBounds(20, 50, 110, 30);
        contentPane.add(label2);

        text2 = new JTextField();
        text2.setBounds(120, 50, 120, 20);
        contentPane.add(text2);

        label3 = new JLabel("Hasil");
        label3.setBounds(20, 80, 110, 30);
        contentPane.add(label3);

        text3 = new JTextField();
        text3.setBounds(120, 80, 120, 20);
        contentPane.add(text3);

        button_jumlah = new JButton("Jumlah");
        button_jumlah.setBounds(120, 125, BUTTON_WIDTH, BUTTON_HEIGHT);
        contentPane.add(button_jumlah);

        button_jumlah.addActionListener(this);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        int bilan1;
        int bilan2;
        int hasill;
        bilan1 = Integer.parseInt(text1.getText());
        bilan2 = Integer.parseInt(text2.getText());
        hasill = bilan1 + bilan2;
        text3.setText(Integer.toString(hasill));
    }

}
